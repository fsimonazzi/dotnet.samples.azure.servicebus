﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Text;

namespace Dotnet.Samples.Azure.ServiceBus
{
    class Program
    {
        private static readonly string MESSAGE = "The quick brown fox jumps over the lazy dog.";
        private static readonly string ENTITY_PATH = "messages/events";
        // INFO: Just to avoid committing sensitive data, replace string.Empty with valid policy:
        private static readonly string CONNECTION_STRING = string.Empty;

        static void Main(string[] args)
        {
            try
            {
                SendMessageToEventHubAsync();
            }
            catch (Exception exception)
            {
                Console.Write(exception.ToString());
            }
            finally
            {
                Console.WriteLine("Press any key to continue . . .");
                Console.ReadKey(true);
            }

        }

        private static async void SendMessageToEventHubAsync()
        {
            var client = EventHubClient.CreateFromConnectionString(CONNECTION_STRING, ENTITY_PATH);
            await client.SendAsync(new EventData(Encoding.UTF8.GetBytes(MESSAGE)));
        }
    }
}
